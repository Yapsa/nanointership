export class Person { 

    id: number;
    name: string;
    username: string;
    age: number;
    work: string;
    position: string;
    adresse: string;

    constructor(id:number, name:string, username:string, age:number, work:string, position:string, adresse:string){
    this.id = id;
    this.name = name;
    this.username = username;
    this.age = age;
    this.work = work;
    this.position = position;
    this.adresse = adresse;
    

    }
  
   } 
 
  