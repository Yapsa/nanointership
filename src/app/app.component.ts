import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Person } from 'src/model/person';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'yestest';

  personne: Person ;
  constructor() {  
    this.personne =  new Person (20,'Basse', 'OUMAR',14,'YOBANTE', 'CTO','DAKAR');
  }

  ngOnInit(): void {
  
  }

  update(eventPerson: { id: number; name: string; username: string; age: number; position: string; work:string; adresse: string;}) { 
    this.personne = eventPerson; 
  } 

}
