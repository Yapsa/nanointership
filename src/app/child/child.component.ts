import { Component, OnInit , Input, EventEmitter, Output} from '@angular/core';
import { Person } from 'src/model/person';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit {


  @Output() public eventPerson = new EventEmitter<Person>(); 
  @Input() person: any;


  changePersonne(){

    console.log('Child component')
    this.person.id =8;
    this.person.name = 'Yafsa';
    this.person.username= 'Yapsee';
    this.person.position= 'Intern';
    this.eventPerson.emit (this.person)
   }

   ngOnInit(): void {
  
  }


}
